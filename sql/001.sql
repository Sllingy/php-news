SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";
CREATE DATABASE IF NOT EXISTS `news` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `news`;

CREATE TABLE IF NOT EXISTS `article`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT,
    `active`      tinyint(1)   NOT NULL,
    `datetime`    datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `author_id`   int(11)      NOT NULL,
    `category_id` int(11)      NOT NULL,
    `title`       varchar(100) NOT NULL,
    `short`       text         NOT NULL,
    `text`        text         NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8;

INSERT INTO `article` (`id`, `active`, `datetime`, `author_id`, `category_id`, `title`, `short`, `text`)
VALUES (1, 1, '2020-06-17 14:25:24', 1, 4, 'Test', '<p>Sports test</p>', '<p>Sports event description</p>');

CREATE TABLE IF NOT EXISTS `author`
(
    `id`       int(11)      NOT NULL AUTO_INCREMENT,
    `name`     varchar(100) NOT NULL,
    `surname`  varchar(100) NOT NULL,
    `email`    varchar(200) NOT NULL,
    `password` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8;

INSERT INTO `author` (`id`, `name`, `surname`, `email`, `password`)
VALUES (1, 'Tom', 'Krelina', 'tom@mail.net', '$2y$10$GPtP/8hh6B6vNZc/3fFkk./np35E6Lgp3NBOWeNECZXrTWqYTKFzC');

CREATE TABLE IF NOT EXISTS `category`
(
    `id`   int(11)      NOT NULL AUTO_INCREMENT,
    `name` varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8;

INSERT INTO `category` (`id`, `name`)
VALUES (4, 'Sports');

CREATE TABLE IF NOT EXISTS `comment`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `article_id` int(11)      NOT NULL,
    `datetime`   datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `name`       varchar(100) NOT NULL,
    `email`      varchar(100) NOT NULL,
    `text`       text         NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8;

INSERT INTO `comment` (`id`, `article_id`, `datetime`, `name`, `email`, `text`)
VALUES (1, 1, '2020-06-17 14:17:10', 'Tim', 'tim@mail.net', 'Cool!\n');
COMMIT;
