<?php

namespace App\Models;

use Nette\Database\Context;

class BaseModel {
    /**
     * @var Context
     */
    public $db;
    private $tableName = '';

    public function __construct($tableName, Context $db) {
        $this->tableName = $tableName;
        $this->db = $db;
    }

    public function delete($id) {
        $selection = $this->get($id);
        if ($selection) {
            return (int)$selection->delete();
        }
        return false;
    }

    public function get($id) {
        return $this->getTable()->get($id);
    }

    public function getTable() {
        return $this->db->table($this->getTableName());
    }

    public function getTableName() {
        return $this->tableName;
    }

    public function getAll() {
        return $this->getTable()->fetchAll();
    }

    public function update($id, $data) {
        return $this->getTable()->wherePrimary($id)->update($this->fixNullValue($data));
    }

    protected function fixNullValue($data) {
        foreach ($data as &$v) {
            if ($v === '') {
                $v = NULL;
            }
        }
        return $data;
    }

    public function insert($data) {
        return $this->getTable()->insert($this->fixNullValue($data));
    }
}