<?php

namespace App\Models;

class AuthorModel extends BaseModel {
    public function getIdsAndFullNames() {
        $result = [];
        foreach ($this->getTable() as $author) {
            $result[$author['id']] = $author['name'] . ' ' . $author['surname'];
        }
        return $result;
    }

    public function getByEmail($email) {
        return $this->getTable()->where('email', $email)->fetch();
    }
}