<?php

namespace App\Models;

class CommentModel extends BaseModel {
    public function getByArticleId($articleId) {
        return $this->getTable()->where('article_id', $articleId)->order('datetime DESC')->fetchAll();
    }
}