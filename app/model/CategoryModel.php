<?php

namespace App\Models;

class CategoryModel extends BaseModel {
    public function getPairs() {
        return $this->getTable()->fetchPairs('id', 'name');
    }
}