<?php

namespace App\Models;

class ArticleModel extends BaseModel {
    public function get5NewestArticles() {
        return $this->getTable()->where('active')->order('datetime DESC')->limit(5)->fetchAll();
    }

    public function getAllOrderedByName() {
        return $this->getTable()->order('title ASC')->fetchAll();
    }

    public function getByCategoryId($categoryId) {
        return $this->getTable()->where('category_id', $categoryId)->where('active')->fetchAll();
    }

    public function getByAuthorId($authorId) {
        return $this->getTable()->where('author_id', $authorId)->where('active')->fetchAll();
    }
}