<?php

namespace App\Presenters;

use Components\ILoginFormFactory;
use Nette;

class BasePresenter extends Nette\Application\UI\Presenter {
    /** @var ILoginFormFactory @inject */
    public $formLogin;

    public function actionLogout() {
        $this->user->logout();
        $this->flashMessage('Logged out');
        $this->redirect('Article:');
    }

    public function createComponentLoginForm() {
        $control = $this->formLogin->create();
        $control->onFormSave[] = function () {
            $this->redirect('Article:');
        };
        return $control;
    }
}
