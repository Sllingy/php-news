<?php

namespace App\Presenters;

use App\Models\ArticleModel;
use App\Models\AuthorModel;
use App\Models\CategoryModel;
use Components\ICategoryFormFactory;

class CategoryPresenter extends BasePresenter {
    /** @var ArticleModel @inject */
    public $modelArticle;
    /** @var CategoryModel @inject */
    public $modelCategory;
    /** @var AuthorModel @inject */
    public $modelAuthor;
    /** @var ICategoryFormFactory @inject */
    public $formCategory;

    public function renderDefault() {
        $this->template->categories = $this->modelCategory->getAll();
    }

    public function renderDetail($id) {
        $this->template->articles = $this->modelArticle->getByCategoryId($id);
        $this->template->category = $this->modelCategory->get($id);
        $this->template->authors = $this->modelAuthor->getAll();
    }

    public function createComponentCategoryForm() {
        $control = $this->formCategory->create();
        $control->onFormSave[] = function ($new) {
            $message = $new ? 'created' : 'edited';
            $this->flashMessage('Category successfully ' . $message);
            $this->redirect('Category:');
        };
        return $control;
    }

    public function actionDelete($id) {
        if (count($this->modelArticle->getByCategoryId($id))) {
            $this->flashMessage("Can't delete a category which contains articles");
        } else {
            $this->modelCategory->delete($id);
            $this->flashMessage('Category successfully deleted');
        }
    }

    public function actionEdit($id) {
        $category = $this->modelCategory->get($id);
        $form = $this->getComponent('categoryForm');
        $form->id = $id;
        $form['form']->setDefaults($category);
    }
}