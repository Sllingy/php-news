<?php

namespace App\Presenters;

use App\Models\ArticleModel;
use App\Models\AuthorModel;
use App\Models\CategoryModel;
use App\Models\CommentModel;
use Components\IArticleFormFactory;
use Components\ICommentFormFactory;

class ArticlePresenter extends BasePresenter {
    /** @var ArticleModel @inject */
    public $modelArticle;
    /** @var CategoryModel @inject */
    public $modelCategory;
    /** @var AuthorModel @inject */
    public $modelAuthor;
    /** @var CommentModel @inject */
    public $modelComment;
    /** @var IArticleFormFactory @inject */
    public $formArticle;
    /** @var ICommentFormFactory @inject */
    public $formComment;

    public function renderDefault() {
        $this->template->articles = $this->modelArticle->get5NewestArticles();
        $this->template->categories = $this->modelCategory->getAll();
        $this->template->authors = $this->modelAuthor->getAll();
    }

    public function renderDetail($id) {
        $form = $this->getComponent('commentForm');
        $form['form']->setDefaults(['article_id' => $id]);
        $this->template->article = $this->modelArticle->get($id);
        $this->template->comments = $this->modelComment->getByArticleId($id);
        $this->template->categories = $this->modelCategory->getAll();
        $this->template->authors = $this->modelAuthor->getAll();
    }

    public function actionDelete($id) {
        $this->modelArticle->delete($id);
        foreach ($this->modelComment->getByArticleId($id) as $comment) {
            $this->modelComment->delete($comment->id);
        }
        $this->flashMessage('Article successfully deleted');
        $this->redirect('Article:administration');
    }

    public function actionDeleteComment($id) {
        $this->modelComment->delete($id);
        $this->flashMessage('Comment successfully deleted');
        $this->redirect('Article:');
    }

    public function createComponentArticleForm() {
        $control = $this->formArticle->create();
        $control->onFormSave[] = function ($new) {
            $message = $new === true ? 'created' : 'edited';
            $this->flashMessage('Article successfully ' . $message);
            $this->redirect('Article:administration');
        };
        return $control;
    }

    public function createComponentCommentForm() {
        $control = $this->formComment->create();
        $control->onFormSave[] = function () {
            $this->flashMessage('Comment successfully created');
            $this->redirect('this');
        };
        return $control;
    }

    public function actionEdit($id) {
        $article = $this->modelArticle->get($id);
        $form = $this->getComponent('articleForm');
        $form->id = $id;
        $form['form']->setDefaults($article);
    }

    public function renderAdministration() {
        $this->template->articles = $this->modelArticle->getAllOrderedByName();
        $this->template->categories = $this->modelCategory->getAll();
        $this->template->authors = $this->modelAuthor->getAll();
    }
}