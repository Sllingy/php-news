<?php

namespace App\Presenters;

use App\Models\ArticleModel;
use App\Models\AuthorModel;
use App\Models\CategoryModel;
use Components\IAuthorFormFactory;

class AuthorPresenter extends BasePresenter {
    /** @var ArticleModel @inject */
    public $modelArticle;
    /** @var AuthorModel @inject */
    public $modelAuthor;
    /** @var CategoryModel @inject */
    public $modelCategory;
    /** @var IAuthorFormFactory @inject */
    public $formAuthor;

    public function renderDefault() {
        $this->template->authors = $this->modelAuthor->getAll();
    }

    public function actionDetail($id) {
        $this->template->articles = $this->modelArticle->getByAuthorId($id);
        $this->template->categories = $this->modelCategory->getAll();
        $this->template->author = $this->modelAuthor->get($id);
    }

    public function createComponentAuthorForm() {
        $control = $this->formAuthor->create();
        $control->onFormSave[] = function ($new) {
            $message = $new === true ? 'created' : 'edited';
            $this->flashMessage('Author successfully ' . $message);
            $this->redirect('Author:');
        };
        return $control;
    }

    public function actionDelete($id) {
        if (count($this->modelArticle->getByAuthorId($id))) {
            $this->flashMessage("Can't delete an author which has articles");
        } else {
            $this->modelAuthor->delete($id);
            $this->flashMessage('Author successfully deleted');
        }
    }

    public function actionEdit($id) {
        $author = $this->modelAuthor->get($id);
        $form = $this->getComponent('authorForm');
        $form->id = $id;
        $form['form']->setDefaults($author);
    }
}