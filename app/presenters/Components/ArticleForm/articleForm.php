<?php

namespace Components;

use App\Models\ArticleModel;
use App\Models\AuthorModel;
use App\Models\CategoryModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

interface IArticleFormFactory {
    function create(): ArticleForm;
}

class ArticleForm extends Control {
    private $modelCategory;
    private $modelArticle;
    private $modelAuthor;
    public $id;
    public $onFormSave;

    function __construct(CategoryModel $modelCategory, AuthorModel $modelAuthor, ArticleModel $modelArticle) {
        parent::__construct();
        $this->modelCategory = $modelCategory;
        $this->modelArticle = $modelArticle;
        $this->modelAuthor = $modelAuthor;
    }

    public function render() {
        $this->template->header = $this->id ? 'Edit article ' : 'Create article';
        $this->template->setFile(__DIR__ . '/articleForm.latte');
        $this->template->render();
    }

    public function createComponentForm() {
        $form = new Form();
        $form->addText('title', 'Title')
            ->setRequired('Please enter a title');
        $form->addTextArea('short', 'Short');
        $form->addTextArea('text', 'Text');
        $form->addCheckbox('active', 'Active');
        $form->addSelect('author_id', 'Author', $this->modelAuthor->getIdsAndFullNames())
            ->setPrompt('~~~~ Choose ~~~~')
            ->setRequired('Please select an author');
        $form->addSelect('category_id', 'Category', $this->modelCategory->getPairs())
            ->setPrompt('~~~~ Choose ~~~~')
            ->setRequired('Please select a category');
        $form->addSubmit('submit', $this->id ? 'Edit' : 'Create');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded($form, $values) {
        if ($this->id) {
            $this->modelArticle->update($this->id, $values);
        } else {
            $this->modelArticle->insert($values);
        }
        $this->onFormSave($this->id == null);
    }
}
