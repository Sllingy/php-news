<?php

use App\Models\AuthorModel;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;

class Authenticator implements IAuthenticator {
    private $modelAuthor;

    public function __construct(AuthorModel $modelAuthor) {
        $this->modelAuthor = $modelAuthor;
    }

    public function authenticate(array $credentials) {
        list($email, $password) = $credentials;
        $user = $this->modelAuthor->getByEmail($email);
        if (!$user) {
            throw new AuthenticationException('Email not found');
        }
        if (!Nette\Security\Passwords::verify($password, $user->password)) {
            throw new AuthenticationException('Invalid password!');
        }
        return new Identity($user->id, ['email' => $user->email]);
    }
}