<?php

namespace Components;

use App\Models\ArticleModel;
use App\Models\CommentModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

interface ICommentFormFactory {
    function create(): CommentForm;
}

class CommentForm extends Control {
    public $modelArticle;
    public $modelComment;
    public $onFormSave;

    function __construct(CommentModel $modelComment, ArticleModel $modelArticle) {
        parent::__construct();
        $this->modelComment = $modelComment;
        $this->modelArticle = $modelArticle;
    }

    public function render() {
        $this->template->header = 'Create comment';
        $this->template->setFile(__DIR__ . '/commentForm.latte');
        $this->template->render();
    }

    public function createComponentForm() {
        $form = new Form();
        $form->addText('name', 'Name')
            ->setRequired('Please enter a name');
        $form->addText('email', 'Email')
            ->setRequired('Please enter an email')
            ->addRule(Form::EMAIL);
        $form->addTextArea('text', 'Text');
        $form->addHidden('article_id');
        $form->addSubmit('submit', 'Add');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded($form, $values) {
        $this->modelComment->insert($values);
        $this->onFormSave();
    }
}
