<?php

namespace Components;

use App\Models\CategoryModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

interface ICategoryFormFactory {
    function create(): CategoryForm;
}

class CategoryForm extends Control {
    private $modelCategory;
    public $id;
    public $onFormSave;

    function __construct(CategoryModel $modelCategory) {
        parent::__construct();
        $this->modelCategory = $modelCategory;
    }

    public function render() {
        $this->template->header = $this->id ? 'Edit category ' : 'Create category';
        $this->template->setFile(__DIR__ . '/categoryForm.latte');
        $this->template->render();
    }

    public function createComponentForm() {
        $form = new Form();
        $form->addText('name', 'Name')
            ->setRequired('Please enter a name');
        $form->addSubmit('submit', $this->id ? 'Edit' : 'Create');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded($form, $values) {
        if ($this->id) {
            $this->modelCategory->update($this->id, $values);
        } else {
            $this->modelCategory->insert($values);
        }
        $this->onFormSave($this->id == null);
    }
}
