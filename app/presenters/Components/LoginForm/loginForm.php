<?php

namespace Components;

use App\Models\AuthorModel;
use Authenticator;
use Exception;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;

interface ILoginFormFactory {
    function create(): LoginForm;
}

class LoginForm extends Control {
    private $modelAuthor;
    public $onFormSave;
    public $user;
    private $authenticator;

    function __construct(AuthorModel $modelAuthor, Authenticator $authenticator, User $user) {
        parent::__construct();
        $this->modelAuthor = $modelAuthor;
        $this->authenticator = $authenticator;
        $this->user = $user;
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/loginForm.latte');
        $this->template->render();
    }

    public function createComponentForm() {
        $form = new Form();
        $form->addEmail('email', 'Email')
            ->setRequired('Please enter an email');
        $form->addPassword('password', 'Password')
            ->setRequired('Please enter a password');
        $form->addSubmit('submit', 'Login');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded($form, $values) {
        try {
            $this->user->login($values['email'], $values['password']);
        } catch (Exception $exception) {
            $this->presenter->flashMessage("Invalid credentials");
        }
        $this->user->setExpiration('14 days');

        $this->onFormSave();
    }
}
