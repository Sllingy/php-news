<?php

namespace Components;

use App\Models\AuthorModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;

interface IAuthorFormFactory {
    function create(): AuthorForm;
}

class AuthorForm extends Control {
    private $modelAuthor;
    public $id;
    public $onFormSave;

    function __construct(AuthorModel $modelAuthor) {
        parent::__construct();
        $this->modelAuthor = $modelAuthor;
    }

    public function render() {
        $this->template->header = $this->id ? 'Edit author ' : 'Create author';
        $this->template->setFile(__DIR__ . '/authorForm.latte');
        $this->template->render();
    }

    public function createComponentForm() {
        $form = new Form();
        $form->addText('name', 'Name')
            ->setRequired('Please enter a name');
        $form->addText('surname', 'Surname')
            ->setRequired('Please enter a surname');
        $form->addEmail('email', 'Email')
            ->setRequired('Please enter an email');
        $form->addPassword('password', 'Password')
            ->setRequired('Please enter a password');
        $form->addSubmit('submit', $this->id ? 'Edit' : 'Create');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded($form, $values) {
        if ($this->id) {
            if ($values['password'] === '') {
                unset($values['password']);
            } else {
                $values['password'] = Passwords::hash($values['password']);
            }
            $this->modelAuthor->update($this->id, $values);
        } else {
            $values['password'] = Passwords::hash($values['password']);
            $this->modelAuthor->insert($values);
        }
        $this->onFormSave($this->id == null);
    }
}
